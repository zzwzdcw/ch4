package com.example.bj02_01;

public class ProgramExpert {
    public String getLanguage(String feature){
        String result;
        switch (feature){
            case "fast":
                result="C/C++";break;
            case "easy":
                result="Python";break;
            case "new":
                result="Kotlin";break;
            case "⚪⚪":
                result="Java";break;
                default:
                    result="You got me";
        }
        return  result;
    }
}
