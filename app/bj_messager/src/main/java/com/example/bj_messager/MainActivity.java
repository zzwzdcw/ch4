package com.example.bj_messager;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        public static final String MESSAGE_KEY="bjfu.it.sun.messager";

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_create_message);
        }
        public void onSendMessage(View Button)
        {
            EditText editText=findViewById(R.id.input);
            String message=editText.getText().toString();
//        Intent intent=new Intent(this,ReceiveMessageActivity.class);
//        intent.putExtra(MESSAGE_KEY,message);
            Intent intent=new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(intent.EXTRA_TEXT,message);
            String chooserTitle=getString(R.string.choser);
            Intent chosenIntent=Intent.createChooser(intent,chooserTitle);
            startActivity(chosenIntent);
        }
    }
}