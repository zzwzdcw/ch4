package com.example.ch4_01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn1=findViewById(R.id.btn1);
        btn1.setOnClickListener(this::onClick);
        Button btn2=findViewById(R.id.btn2);
        btn2.setOnClickListener(this::onClick);
        Button btn3=findViewById(R.id.btn3);
        btn3.setOnClickListener(this::onClick);
    }

    public void onClick(View v) {
        int id = v.getId();
        Intent intent = new Intent();
        switch (id) {
            case R.id.btn1:
                intent.setAction(Intent.ACTION_DIAL);  //拨号
                intent.setData(Uri.parse("tel:13781652561"));  //意图数据=>打电话
                /*intent.setAction(Intent.ACTION_VIEW);//用于显示用户的数据，会根据用户的数据类型打开相应的Activity
                intent.setData(Uri.parse("tel:15527643858"));*/
                startActivity(intent);
                break;
            case R.id.btn2:  //调用系统发短信程序
                intent.setAction(Intent.ACTION_SENDTO); //意图动作
                intent.setData(Uri.parse("sms:13781652561?body=手机短信测试"));  //意图数据=>发短信
                startActivity(intent);
                break;
            case R.id.btn3:  //调用系统照相程序
                intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);  //意图动作
                startActivity(intent);
                break;
        }
    }
}